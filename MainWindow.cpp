#include "MainWindow.hpp"
#include <iostream>
#include "moc_MainWindow.cpp"

MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags flags)
    : QMainWindow(parent, flags) {
  ui_.setupUi(this);
  ui_.pushButton->setAutoDefault(false);
  ui_.pushButton2->setAutoDefault(false);

  connect(ui_.pushButton, SIGNAL(clicked()), this, SLOT(pushButtonClicked()));
  connect(ui_.pushButton2, SIGNAL(clicked()), this, SLOT(pushButton2Clicked()));
}

void MainWindow::pushButtonClicked() {
  QMessageBox msg;
  msg.setText("Hello");

  int ret = msg.exec();
}

void MainWindow::pushButton2Clicked() {
  QMessageBox msg;
  msg.setText("World");

  int ret = msg.exec();
}
