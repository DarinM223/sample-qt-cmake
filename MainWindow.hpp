#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QtWidgets>
#include "ui_mainwindow.h"

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(QWidget* parent = 0, Qt::WindowFlags flags = 0);

 private slots:
  void pushButtonClicked();
  void pushButton2Clicked();

 private:
  Ui_MainWindow ui_;
};

#endif
